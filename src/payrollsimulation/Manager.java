/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payrollsimulation;

public class Manager extends Employee {
    
    private double bonus;
    
    public Manager(String name, double hourlyWage, double hoursWorked, 
            double bonus){
        
        this.name = name;
        this.hourlyWage = hourlyWage;
        this.hoursWorked = hoursWorked;
        this.bonus = bonus;
    }
    
    public double getBonus(){
        return bonus;
    }
    
    public void setBonus(double bonus){
        this.bonus = bonus;
    }
    
    public double calculatePay(){
        return hoursWorked * hourlyWage + bonus;
    }
}
