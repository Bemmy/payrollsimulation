/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payrollsimulation;

public class PayrollSimulation {

    public static void main(String[] args) {
        
        Employee employee = new Employee("Adam Parks",  14.50, 50);
        Employee manager = new Manager("Skylar Thomas", 22.50, 60, 300);
        
        System.out.println("Name: " + employee.getName() + " Paycheque: $" +
                employee.calculatePay() + "\nName: " + manager.getName() +
                " Paycheque: $" + manager.calculatePay());
        
    }
    
}
