/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payrollsimulation;

public class Employee {
    
    protected String name;
    protected double hourlyWage;
    protected double hoursWorked;
    
    public Employee(){}
    
    public Employee(String name, double hourlyWage, double hoursWorked){
        this.name = name;
        this.hourlyWage = hourlyWage;
        this.hoursWorked = hoursWorked;
    }
    
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public double getHourlyWage(){
        return hourlyWage;
    }
    
    public void setHourlyWage(double hourlyWage){
        this.hourlyWage = hourlyWage;
    }
    
    public double getHoursWorked(){
        return hoursWorked;
    }
    
    public void setHoursWorked(double hoursWorked){
        this.hoursWorked = hoursWorked;
    }
    
    public double calculatePay(){
        return hoursWorked * hourlyWage;
    }
}
